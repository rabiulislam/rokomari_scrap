module github.com/rabiulislam993/rokomari_scrap

go 1.13

replace github.com/rabiulislam993/rokomari_scrap => ./rokomari_scrap

require (
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/gocolly/colly/v2 v2.0.1
)
